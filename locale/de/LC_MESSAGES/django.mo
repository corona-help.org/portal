��    :      �  O   �      �     �          #  B   <  D        �     �     �     �     �                     )     1     N     ^     e     u     �     �     �  	   �     �     �     �       8        Q     ]     f     o     x     �  
   �     �     �     �     �     �  !   �                 	   +     5     O     T  
   Z     e  	   i     s     x     }  	   �     �     �  B  �     �	  $   �	  #   
  X   ;
  Y   �
     �
     �
          +     1     F     K     S     \  "   e     �     �     �  
   �     �     �     �        	   	               <  3   X     �     �     �     �     �  
   �     �     �     �          !     8     D     a     n  
   t          �     �     �     �     �     �     �     �     �     �     �  	   �            -                    	   2                        &           ,         "      7   4       (   .         3                  '       8       +                      5       *             1   !       9   6   0   #                $                  )      
         %      /   :    >> Upload photo here Accept Privacy Terms Accept Service Agreement Activate SMS notifications if someone needs help in my area (~5km) Activate email notifications if someone needs help in my area (~5km) Arabisch Bring things from A to B Change password: %s City Confirm Password Country Deutsch Englisch English Enter a valid email address. Forgot password German Help Categories House Number Identity Card Number Identity Card back site Identity Card front site Languages Log in Password Password changed successfully. Please enter SMS Token Please type in format +49(leave out leading 0) 152337182 Postal Code Register Reset it Russisch Spanisch State Streetname Talk to people on the phone Task This field is required. Token is invalid Type of the Task Which languages are you speaking? ZIP code active animal care childcare currently being worked on done email first name fon last name once open other recurring send shopping Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Hier Foto hochladen Zustimmung zur Datenschutzerklärung Zustimmung zu den Nutzungsbedingung Aktiviere SMS-Benachrichtigungen wenn jemand in meiner Nähe Hilfe braucht (5km Umkreis) Aktiviere Emailbenachrichtigungen wenn jemand in meiner Nähe Hilfe braucht (5km Umkreis) Arabisch Bringe Sachen von A nach B Ändere das Passwort: %s Stadt Passwort wiederholen Land Deutsch Englisch Englisch Gib eine gültige Emailadresse ein Passwort vergessen Deutsch Hilfe Kategorien Hausnummer Ausweisnummer Rückseite des Ausweises Vorderseite des Ausweises Sprachen Einloggen Passwort Password erfolgreich geändert. Bitte gebe den SMS-Code ein Bitte im Format +49(erste 0 weglassen)15838829 z.B. Postleitzahl Registrieren zurücksetzen Russisch Spanisch Bundesland Straßenname Da sein zum Sprechen am Telefon Aufgabe Dieses Feld ist notwendig. Der Code ist ungültig Aufgabentyp Welche Sprachen sprichst Du? Postleitzahl aktiv Tierpflege Kinderbetruung wird gerade erledigt erledigt email Vorname Telefon Nachname einmalig offen andere wiederkehrend absenden einkaufen 