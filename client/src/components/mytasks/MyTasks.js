import React, { useState } from 'react';
import './styles.scss';
import { ArrowLeft } from 'react-feather';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { logout } from '../../redux/actions/user.actions';
import MyTaskBox from './components/MyTaskBox';

const MyTasks = ({setShowMyTasks}) => {
  const { t } = useTranslation();
  const myTasks = useSelector(redux => redux.myTasks);
  const user = useSelector( redux => redux.user);
  const dispatch = useDispatch();
  const onGoing = myTasks.filter(item => item.status ==='ongoing');
  const done = myTasks.filter(item => item.status === 'done');
  const [open, setOpen] = useState();

  return (
    <div className="myTasksWrapper">
      <div className="header">
        <button className="btn-myTasks btn-action" onClick={() => setShowMyTasks(false)}>
          <ArrowLeft style={{marginRight: '10px'}}/>
          {t('back')}
        </button>
        <h3>
          <div>{t('myTasks.yourTasks')}</div>
        </h3>
        <button className="btn-myTasks btn-action" onClick={() => dispatch(logout())}>
          {t('header.logout')}
        </button>
      </div>
      <div className="myTasks">
      <div className="categories">
        <h4>{t('myTasks.onGoing')}</h4>
          {onGoing.map(item => <MyTaskBox key={item.id} content={item}/>)}
        </div>
        <div className={`categories${open ? ' open': ''}`}>
          <h4 onClick={() => setOpen(!open)}>{t('myTasks.done')}</h4>
        {done.map(item => <MyTaskBox key={item.id} content={item}/>)}
        </div>
      </div>
      <div className="myDetailsWrapper">
        <h3>
          {t('myTasks.myDetails')}
        </h3>
        <div className="myDetails">
        <span>{`${user.first_name} ${user.last_name ? user.last_name : ''}`}</span><br />
        {user.email &&user.email}<br />
        {user.fon && user.fon}<br />
        {user.street_name+' '+(user.street_number ? user.street_number : '')}<br />
        {user.postal_code+' '+user.city}
        </div>
        <button className="btn-myTasks btn-action" style={{padding: '11px 50px', display: 'block', margin: '0 auto'}}>
          {t('myTasks.edit')}
        </button>
      </div>
    </div>
  );
}

export default MyTasks;
