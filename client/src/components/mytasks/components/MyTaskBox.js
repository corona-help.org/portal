import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { updateMyTask } from '../../../redux/actions/myTasks.actions';
import './styles.scss';
import { ChevronRight, RefreshCw, CheckCircle, MapPin, Phone, Trash2 } from 'react-feather';

const MyTaskBox = ({content}) => {
  const { t } = useTranslation();
  const { title, status, description, id } = content;
  const { person_in_need } = content;
  const { street_name, postal_code, city, fon} = person_in_need;
  const dispatch = useDispatch();
  const pTask = person_in_need.tasks.find(i => i.id = id);
  const { type } = pTask;
  const [open, setOpen] = useState();

  const updateTask = (id, status) => {
    dispatch(updateMyTask(id, status));
}

  return (
    <div className={`myTaskContainer${status=== 'done' ? ' done' :''}`}>
      <h3 className={open ? 'isOpen' : ''} style={{display: 'flex'}} onClick={() => setOpen(!open)}>
        <ChevronRight style={{minWidth: '24px'}}/><div className="title">{title}</div>
      </h3>
      <div style={{
        display: open ? 'block': 'none',
      }}>
        <div style={{borderBottom:'1px solid rgb(151, 151, 151, 0.31)', width: '120%', transform: 'translateX(-20px)', marginTop: '13px'}}/>
        <div style={{ marginTop: '12px' }}>
        {type === 'regular' && (<>

              <span className="tasktype">            <RefreshCw size="14" />{t('localactivist.recurring_task')}</span>
              </>)
              }
              {type === 'once' && (<>

              <span className="tasktype">            <CheckCircle size="14" />{t('localactivist.one_time_task')}</span>
              </>)
              }
              <div className="myTaskIcon" style={{ display: 'flex', marginTop: '11px', marginLeft: '2px', marginBottom: '12px'}}>
                  <MapPin size="22" color="#2f2f2f"/>
                  <div 
                  style={{
                      fontSize: '14px',
                      marginLeft: '5px',
                      lineHeight: '1.1em',
                  }}> 
                      {street_name}<br /> {postal_code} {city}
                  </div>
              </div>
              <div className="myTaskIcon" style={{ display: 'flex', marginTop: '8px', marginBottom: '12px', marginLeft: '2px' }}>
                  <Phone size="22" color="#2f2f2f"/>
                  <div 
                  style={{
                      fontSize: '14px',
                      marginLeft: '5px',
                      lineHeight: '1.1em',
                      marginTop: '5px',
                  }}> 
                      {fon}
                  </div>
              </div>
              <div className="myTasksDescription">
                {description}
              </div>
              {status === 'ongoing' && (
                <>
 <button
                onClick={() => updateTask(id, 'done')}
                className="myTaskDarkButton"
              >
                {t('myTasks.markAsDone')}
              </button>
              <div style={{textAlign: 'right'}}>
              <button
              onClick={() => updateTask(id, 'open')}
              className="btn"
              style={{
              background: 'none',
              fontWeight: 'bold',
              fontSize: '0.8em',
              border: 'none',
              color: '#e85555',
              float: 'right',
              marginTop: '19px',
          }}><Trash2 size="20" color='#e85555' style={{marginTop: '-5px'}} /> {t('tasks.cancel')}</button>
          </div>
                </>
              )}
             
        </div>
      </div>
    </div>
  )

}

export default MyTaskBox;
