import React, { useState, useEffect } from "react";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  Button
} from "reactstrap";
import { MapPin } from "react-feather";
import { useTranslation } from "react-i18next";
import FilterDropDown from "./FilterDropDown";
import axios from "axios";
import { API_SERVER } from '../../config';
import map from "../map";
import "./styles.scss";
import TaskItemBox from "../taskbox/taskitembox";
import { useSelector } from "react-redux";



const SearchBar = ({ selectedFilters, handleFilterChange, handleStartSearch }) => {
  const { t } = useTranslation();
  const [input, setInput] = useState("");
  const mapTasks = useSelector(redux => redux.mapTasks);
  const [showResults, setShowResults] = useState(false);
  const options = [
    { name: t("tasks.shopping"), id: "shopping" },
    { name: t("tasks.animal_care"), id: "animal_care" },
    { name: t("tasks.fon_care"), id: "fon_care" },
    { name: t("tasks.logistics"), id: "logistics" },
    { name: t("tasks.babysitting"), id: "babysitting" },
    { name: t("tasks.other"), id: "other" }
  ];

  const handleEventFilterChange = async values => {
    handleFilterChange(values);

  };
  const handleEventLocationChange = async values => {
    setInput(values.currentTarget.value)

  }
  const handleSearchClick = (e) => {
    setShowResults(!showResults);
    handleStartSearch(input)

  }

  useEffect(()=> {
    if (showResults === false && input) {
      setTimeout(()=> setShowResults(true), 800);
    }
  },[showResults]);

  return (
    <div className="searchbar">
      <h2 className="searchbar__title">{t("searchbar.title")}</h2>
      <div className="filter-wrapper">
        <InputGroup className="searchbar__inputgroup">
          <InputGroupAddon addonType="prepend">
            <InputGroupText className="searchbar__input-icon">
              <MapPin size="20" />
            </InputGroupText>
          </InputGroupAddon>
          <Input
            className="searchbar__input"
            placeholder={t("searchbar.inputPlaceholder")}
            onChange={handleEventLocationChange}
            value={input}
            onKeyUp={(e) => {  if (e.keyCode === 13) handleSearchClick()}}
          />
        </InputGroup>
        <FilterDropDown
          options={options}
          selectedFilters={selectedFilters}
          onChangeFilter={handleEventFilterChange}
        />
        <Button className="btn btn-action" onClick={handleSearchClick}>{t("searchbar.search")}</Button>
      </div>
      <div style={{marginLeft: '55px', marginTop: '5px', minHeight: '24px'}}>{(showResults && input && mapTasks && mapTasks.length > -1) && t('searchbar.foundXResult', {count: mapTasks.length}) }</div>
    </div>
  );
};

export default SearchBar;
