import React, { Fragment, useState } from "react";
import { useTranslation } from "react-i18next";
import { Phone, Info } from "react-feather";
import Header from "../header";
import SearchBar from "../searchbar";
import Map from "../map";
import { useViewType, VIEW_TYPE } from '../../hooks';
import InfoGraphic from "../../styles/images/infographic.svg";
import { LoginButton, RegisterButton, HotlineBanner } from '../common';
import "./styles.scss";
import { useSelector } from "react-redux";
import Login from "../header/components/Login";
import MyTasks from "../mytasks/MyTasks";


const LandingPage = () => {
  const { t } = useTranslation();
  const user = useSelector(redux => redux.user);
  const [selectedFilters, setSelectedFilters] = useState([]);
  const [startSearch, setStartSearch] = useState([]);
  const [showLoginForm, setShowLoginForm] = useState(false);
  const [showMyTasks, setShowMyTasks] = useState(false);
  const [viewType] = useViewType();


  return (
    <Fragment>
      { (viewType === VIEW_TYPE.DESKTOP || (viewType === VIEW_TYPE.MOBILE && !showMyTasks)) && <Header setShowMyTasks={setShowMyTasks}/>}
      { (showMyTasks && user && user.loggedIn) && <MyTasks setShowMyTasks={setShowMyTasks}/>}
      { (!showMyTasks || !user || !user.loggedIn ) && (
        <>
      <SearchBar
        selectedFilters={selectedFilters}
        handleFilterChange={setSelectedFilters}
        handleStartSearch={setStartSearch}
      />
      {showLoginForm && (
        <Login setShowLoginForm={setShowLoginForm} />
      )}
      <Map tasktype={selectedFilters} startsearch={startSearch} />
      {(viewType === VIEW_TYPE.MOBILE && !user.loggedIn) &&
        <div className="landingpage__mobileButtons">
          <RegisterButton />
          <LoginButton onClick={() => setShowLoginForm(!showLoginForm)}/>
          <HotlineBanner />
        </div>
      }
      <header className="landingpage__header">
        <h1> {t("landingpage.title")} </h1>
      </header>
      <section className="section-one">
        <div className="intro-notes">
          <p>
            {t("landingpage.sectionone_p1")}
            <br />
            <br />
            {t("landingpage.sectionone_p2")} <br />
            <br />
            <a href="https://corona-help.org/">corona-help.org </a>
            {t("landingpage.sectionone_p3")}
          </p>
        </div>
        <div className="boxes">
          <div className="box hotline-box">
            <h2>{t("landingpage.hotlineBoxTitle")} </h2>
            <div className="hotline-in-box">
              <Phone size="30" />
              <p> 06626 8099862 </p>
            </div>
          </div>
          <div className="box help-box">
            <h2> {t("landingpage.helpBoxTitle")} </h2>
            <p>{t("landingpage.helpBoxText")}</p>
            <br />
            <button type="button" className="btn btn-help">
              {" "}
              0662 68099862{" "}
            </button>
          </div>
        </div>
      </section>
      <section className="section-two">
        <img src={InfoGraphic} width="80%" height="auto" alt="infographics" />
        <div className="info-infographic">
          <Info size="20" />
          <p>{t("landingpage.sectiontwo_info")}</p>
        </div>
      </section>
      <section className="section-three">
        <h1>{t("landingpage.sectionthree_title")}</h1>
        <br />
        <p> {t("landingpage.sectionthree_alt")}</p>
      </section>
      </>
      )
    }
    </Fragment>
  );
};

export default LandingPage;
