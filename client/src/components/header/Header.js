import React, { useState } from "react";
import { LoginButton, RegisterButton, HotlineBanner } from '../common';
import Logo from "../../styles/images/logo.svg";
import Login from './components/Login';
import Logout from './components/Logout';
import { useViewType, VIEW_TYPE } from '../../hooks';
import './styles.scss';
import { useSelector } from "react-redux";
import { ChevronDown } from "react-feather";
import { useTranslation } from "react-i18next";
import MyTasksButton from "../common/myTasksButton/MyTasksButton";

const Header = ({setShowMyTasks}) => {
    const {t} = useTranslation();
    const [viewType] = useViewType();
    const [showLoginForm, setShowLoginForm] = useState();
    const [showLogout, setShowLogout] = useState();
    const user = useSelector(state => state.user);

    //if (viewType === VIEW_TYPE.MOBILE) return null;

    const handleClick = (e) => {
        setShowLoginForm(!showLoginForm);
    }
    return (
        <>
        {(viewType !== VIEW_TYPE.MOBILE || user.loggedIn) && (
            <nav className="navbar" style={viewType !== VIEW_TYPE.MOBILE ? {} : {backgroundColor: 'rgba(255, 229, 107,0.3)'}}>
            {viewType !== VIEW_TYPE.MOBILE && (
            <div className="navbar-leftpart">
            <a className="navbar-logo" href="#">
                <img src={Logo} width="auto" height="40" alt="logo" />
            </a>
            <HotlineBanner />
        </div>
            ) }

            <div className="navbar-buttons" style={viewType === VIEW_TYPE.MOBILE ? {width: '100%'} : {}}>
                { (user && user.loggedIn) && (
                    <>
                        <div style={{
                            display: 'inline-block',
                            position: 'relative',
                            fontSize: '18px',
                            fontWeight: 700,
                            color: '#2F2F2F',
                            marginRight: '17px',
                            flexGrow: viewType === VIEW_TYPE.MOBILE ? 1 : 0,
                            display: 'flex',
                            alignItems: 'center',
                            }}
                        >
                         {t('header.greeting')+' '+user.first_name}
                        </div>
                        <MyTasksButton setShowMyTasks={setShowMyTasks}/>
                        <button style={{
                            background: 'none',
                            border: 'none',
                        }}>
                            <ChevronDown onClick={()=>setShowLogout(true)}/>
                        </button>
                        {showLogout && (
                            <Logout setShowLogout={setShowLogout}/>
                        )}
                    </>
                )
                }
                { (!user || !user.loggedIn) && (
                                        <>
                                        <div style={{display: 'inline-block', position: 'relative'}}>
                                            <LoginButton onClick={handleClick}/>
                                            {showLoginForm && <Login setShowLoginForm={setShowLoginForm}/>}
                                        </div>
                                        <RegisterButton />
                                    </>
                )}
            </div>
        </nav>
        )}
</>
    );
}

export default Header;
