import React, { useState, useEffect} from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch} from 'react-redux';
import { useViewType, VIEW_TYPE } from '../../../hooks';

import './styles.scss';
import { login } from '../../../redux/actions/user.actions';

const Login = ({setShowLoginForm}) => {
  const dispatch = useDispatch();
  const [viewType] = useViewType();

  const { t } = useTranslation();
  const [form, setForm] = useState({});
  const [error, setError] = useState();
  const handleLogin = async (e) => {
    e.preventDefault();
    const loggedIn = await dispatch(login(form));
    if (loggedIn && !loggedIn.detail) {setShowLoginForm(false);}
    else {setError(loggedIn.detail)}
    console.log(loggedIn.detail)
  }
  const listenClicks = (e) => {
    const parent = document.getElementById('login-form');
    if (e.target.id === 'login-form' || (parent && parent.contains(e.target))) {
    } else {
      setShowLoginForm(false);
    }
}


  const handleChange = (e) => {
    setForm({...form, [e.target.name]: e.target.value});
  }

  useEffect(() =>{
    window.addEventListener('click', listenClicks);
    return () => window.removeEventListener('click', listenClicks);
  },[])

  return (
    <div className="login-form" id="login-form"
    style={viewType === VIEW_TYPE.MOBILE ?{
      position: 'fixed',
      marginLeft: '50%',
      zIndex:100000,
      transform: 'translate(-50%, -50%)',
      top: '50%',
    } :{}}>
      <h2>{t('login.title')}</h2>
      <form>
      <label htmlFor="email">
        {t('login.emailAddress')}
        <br />
        <input type="text" name="email" id="email" value={form.email} onChange={handleChange} />
      </label>
      <label htmlFor="password">
        {t('login.password')}
        <input type="password" name="password" id="password" value={form.password} onChange={handleChange} />
      </label>
  {error && (<small style={{color: 'red'}}>{error}</small>)}
      <button className="btn-action" onClick={handleLogin}>
        {t('login.login')}
      </button>
      </form>
  <div className="register">{t('login.noAccount')} <a to="#">{t('login.registerHere')}</a></div>
    </div>
  )
}

export default Login;