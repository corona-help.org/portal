import React, { useEffect, useState } from 'react';
import axiosIntercepts from '../helper';

const Initialize = (props) => {
  const [ready, setReady] = useState();

  const init = async () => {
    await axiosIntercepts()

    setReady(true)
  }
  useEffect( ()=> {
    init()
  }, [])

  if (!ready) return null;

  return (
    <>
    {props.children}
    </>
  )
}

export default Initialize;