import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {useTranslation} from "react-i18next";
import { Provider } from 'react-redux';

import configureStore from '../redux/store';
import LandingPage from './landingPage';
import CookieConsent from './cookieConsent';
import Footer from './footer';
import '../styles/reset.scss';
import { isAuthenticated } from '../redux/actions/user.actions';
import Initialize from './Initialize';



const Spinner = () => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const user = useSelector(state => state.user);
        // this should check if the user can do a login before every other site construction start and
    // offer this information for other components
    useEffect(() => dispatch(isAuthenticated()),[])
    if (!user || user.loggedIn === undefined) {
        return(
            <div style={{position: 'fixed',
                top: 0,
                left: 0,
                width: '100%',
                height:'100%',
                backgroundColor: 'rgba(0,0,0,0.3',
                zIndex:1000,
            }}>
                <div style={{
                    width: '100%',
                    maxWidth: '300px',
                    height: '100%',
                    textAlign: 'center',
                    maxHeight: '200px',
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%,-50%)',
                    background: 'white',
                    borderRadius: '5px',
                    paddingTop:' 85px',
                }}
                >
                {t('landingpage.applicationLoading')}
                </div>
            </div>
        );
    }

    return null;

};

const App = () => {

    const initialState = {};
    const store = configureStore(initialState);

    return (
            <div className="App">
                <Initialize >
                    <Provider store={store}>
                    <Spinner />
                    <LandingPage/>
                    <Footer/>
                    <CookieConsent/>
                    </Provider>
                </Initialize>
            </div>
    );
}

export default App;
