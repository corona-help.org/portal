import React, { useEffect, useState, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import _ from 'underscore'
import { MAP_API_KEY } from '../../config';
import { API_SERVER } from '../../config';
import TaskBox from "../taskbox";
import axios from "axios";
import './styles.scss';
import {useTranslation} from "react-i18next";
import getMapTasks from '../../redux/actions/mapTasks.actions';


delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('../../styles/images/marker.svg'),
    iconUrl: require('../../styles/images/marker.svg'),
});

function tasktypesAdString (values){
    if (values.length === 0){
        return ""
    }
    let strg =""
    for (let i = 0; i < values.length; i++) {
        strg = strg + values[i].id+","; // value
    }
    return "type="+encodeURIComponent(encodeURIComponent(strg))
}

const fetchPersonInNeed = async (id) => {
    if (!id) return;
    const person = await axios.get(`${API_SERVER}localactivist/api/v1/personinneed/${id}/`)
    .then(res => res.data)
    .catch(err => console.log(err));
    return person;
}
const fetchTaskDetails = async (id) => {

}

const Map = ({ tasktype, startsearch }) => {
    const { t } = useTranslation();
    const [markerData, setMarkerData] = useState([]);
    const [markers, setMarkers] = useState([]);
    const mymap = useRef();
    const user = useSelector(state => { return state.user });
    const [personInNeed, setPersonInNeed] = useState();
    const dispatch = useDispatch();

    const handleClick = async (e) => {
        const personInData = markerData.data.data[e.target.myCustomID];
        if (user && user.loggedIn) {
            const personInNeed = await fetchPersonInNeed(personInData.id);
            setPersonInNeed(personInNeed);
        }
    }

    const fetchMarkers = async () => {
        let location =  encodeURIComponent(encodeURIComponent(startsearch))
        if (location.length ===0){
            location = t('map.startingpoint')
        }
        const markerData = await axios.get(API_SERVER + 'api/v1/initial_data/?location='+location+'&format=json&'+ tasktypesAdString(tasktype));
        dispatch(getMapTasks(markerData.data.data));
        setMarkerData(markerData);
    };

    const listenClicks = (e) => {
        const parent = document.getElementById('taskBox');
        const swal2 = document.getElementsByClassName('swal2-container')[0];
        if (e.target.id !== 'taskBox' && !e.target.classList.contains('leaflet-marker-icon') && (parent && !parent.contains(e.target))) {
            if (!swal2 || (swal2 && !swal2.contains(e.target))) {
                setPersonInNeed();
            } else {

            }
        }
    }

    useEffect(() => {
        mymap.current = L.map('mapid', {scrollWheelZoom: false}).setView([51.1642292, 10.4541194], 6);

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                mymap.current.setView([position.coords.latitude, position.coords.longitude], 10);
            });
        }

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: MAP_API_KEY
        }).addTo(mymap.current);
        
        fetchMarkers();
        window.addEventListener('click', listenClicks)
        return () => {
            window.removeEventListener('click', listenClicks);
        }
        }, []);
    
    useEffect(()=>{
        if (tasktype.length!==0){
            fetchMarkers()
        }

    },[tasktype]);

    useEffect(()=>{
        if (startsearch.length >0){
            fetchMarkers();
        }
    },[startsearch]);

    useEffect(()=> {
        let flagToRefreshMapObject = false;
        mymap.current.eachLayer(item => {
            flagToRefreshMapObject = true;
        })

        if (flagToRefreshMapObject) {
            markers.forEach(item => {
                mymap.current.removeLayer(item);
            })
        }
    },[markerData, user])

    useEffect(() => {
        const icon_paths = {
            "someone_needs":require('../../styles/images/dog.svg'),
            "shopping":require('../../styles/images/shopping-cart.svg'),
            "animal_care":require('../../styles/images/dog.svg'),
            "fon_care":require('../../styles/images/phone.svg'),
            "logistics":require('../../styles/images/truck.svg'),
            "babysitting":require('../../styles/images/smile.svg'),
            "other":require('../../styles/images/star.svg'),
        };


        const unselectedmyIcon = L.icon({
            iconUrl: require('../../styles/images/marker.svg'),
            iconRetinaUrl: require('../../styles/images/marker.svg'),
            iconSize: [30, 40],
            iconAnchor: [30, 20],
            popupAnchor: [-12, -15],
            shadowSize: [68, 95],
            shadowAnchor: [22, 94]
        });
        function createMarkerFromItem(item, index){

            const content = document.createElement('div');
            content.className="innerPopup";
            const header2 = document.createElement("h2");
            header2.textContent = t('map.help_needed_here');
            content.appendChild(header2);
            _.each(item.tasks, (thetask) => {
                if (thetask.status === "open"){
                    const taskContainer = document.createElement('div');
                    taskContainer.className = 'taskContainer';
                    const icon = document.createElement('img');
                    icon.src = icon_paths[thetask.help_topics.name];
                    taskContainer.appendChild(icon);
                    const taskSpn = document.createElement("span");
                    taskSpn.className = 'taskSpn';
                    const langTrans = 'tasks.'+thetask.help_topics.name;
                    taskSpn.innerText = t(langTrans);
                    taskContainer.appendChild(taskSpn);
                    content.appendChild(taskContainer);
                }
            });
            const button = document.createElement('button');
            button.textContent =  t('map.help_here');
            button.className = "action-button btn";
            button.onclick = ()=> {window.location=API_SERVER+'registration/'};
            content.appendChild(button);
            const marker =  L.marker([item.lat, item.long],{icon:unselectedmyIcon}).on('click', handleClick);
            marker.myCustomID = index;

            if (!user.loggedIn) marker.bindPopup(content);

            return marker
            }

            if (markerData.hasOwnProperty("data")){
                let tempMarkers = [];
                _.each(markerData.data.data, function (item, index) {
                    let mar = createMarkerFromItem(item, index);
                    tempMarkers.push(mar);
                    mymap.current.addLayer(mar);
                });
                if (startsearch.length >0){
                    mymap.current.flyTo(L.latLng(markerData.data.geo.lat, markerData.data.geo.lng), 13)
                } else {
                    mymap.current.flyTo(L.latLng(markerData.data.geo.lat, markerData.data.geo.lng))
                }

                setMarkers(tempMarkers);
            }


    }, [markerData, user]);

    let protectedComponent = null;
    if (user.loggedIn && personInNeed) {
        protectedComponent = <TaskBox personInNeed={personInNeed}/>;
    }
    return (

        <div>
            {protectedComponent}
            <div id="mapid"></div>

        </div>
    )
}

export default Map;