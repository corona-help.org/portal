import React from 'react';
import { useTranslation } from 'react-i18next';
import { useCookie } from '../../hooks';
import './styles.scss';

const CookieConsent = () => {
    const { t } = useTranslation();
    const [cookieConsent, setCookieContent] = useCookie('cookie_consent');

    if (cookieConsent || cookieConsent === false) return null;

    return (
        <div className="cookies">
            <p> {t('cookieConsent.useCookies')} <a href="/privacy/"> {t('cookieConsent.moreInfo')}</a></p>
            <button type="button" className="btn btn-cookies" onClick={() => setCookieContent(false)}>{t('cookieConsent.reject')}</button>
            <button type="button" className="btn btn-cookies" onClick={() => setCookieContent(true)}>{t('cookieConsent.accept')}</button>
        </div>
    );
}

export default CookieConsent;