import React from 'react';
import { useTranslation } from 'react-i18next';
import './styles.scss';

const Footer = () => {
    const { t, i18n } = useTranslation();

    const changeLanguage = ev => {
        i18n.changeLanguage(ev.currentTarget.value);
    }

    return (
        <footer className="footer">
            <a href="/about/">{t('footer.about_us')}</a>
            <a href="participate/">{t('footer.participate')}</a>
            <a href="/privacy/">{t('footer.privacy')}</a>
            <a href="/terms-service/">{t('footer.service_terms')}</a>
            <a href="/imprint/">{t('footer.imprint')}</a>
            <select id="language" name="language" onChange={changeLanguage} value={i18n.language}>
                <option value="de">DE </option>
                <option value="en">EN </option>
            </select>
        </footer >
    );
}

export default Footer;