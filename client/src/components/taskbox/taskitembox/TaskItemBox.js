import React, { useState, useEffect } from 'react';
import './styles.scss';
import { useTranslation } from 'react-i18next';
import { RefreshCw, MapPin, CheckCircle, Phone, Trash2} from "react-feather";
import Description from "./description";
import { updateMyTask } from '../../../redux/actions/myTasks.actions';
import { useDispatch } from 'react-redux';
import Swal from 'sweetalert2';

const TaskItemBox = ({taskDetails, personInNeed}) => {
    const { t } = useTranslation();
    const { title, type, id, description, street_name, postal_code, city, status, phone } = taskDetails;
    const [accepted, setAccepted] = useState();
    const dispatch = useDispatch();

    useEffect(() => {
        if (status === 'ongoing') {
            setAccepted(true);
        } else {
            setAccepted(false);
        }    
    },[status])

    const updateTask = (id, status) => {
        dispatch(updateMyTask(id, status, personInNeed));
    }

    const verifyAcceptance = (id, status) => {
        Swal.fire({
            title: t('tasks.acceptTaskTitle'),
            text: t('tasks.acceptTask'),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: t('tasks.iConfirm'),
            cancelButtonText: t('tasks.cancel'),
        }).then(()=> {
            updateTask(id, status);
        }).catch(()=> {});
    }

    return (
        <div className="taskItemBox" style={accepted ? { backgroundColor: '#fff4c3' }: {}}>
            {accepted && <span style={{color: "#0e6d85", fontWeight: 'bold', marginRight: '20px'}}>{t('tasks.accepted')}</span>}
            {type === 'regular' && (<>
            <RefreshCw size="14" />
            <span className="tasktype">{t('localactivist.recurring_task')}</span>
            </>)
            }
            {type === 'once' && (<>
            <CheckCircle size="14" />
            <span className="tasktype">{t('localactivist.one_time_task')}</span>
            </>)
            }
            <h3>{title}</h3>
            <div style={{ display: 'flex', marginTop: '7px', marginLeft: '2px', marginBottom: '12px'}}>
                <MapPin size="22" color="#2f2f2f"/>
                <div 
                style={{
                    fontSsize: '1.1em',
                    marginLeft: '5px',
                    lineHeight: '1.1em',
                    marginTop: '5px',
                }}> 
                    {street_name}<br /> {postal_code} {city}
                </div>
            </div>
            { accepted && (
            <div style={{ display: 'flex', marginTop: '8px', marginBottom: '12px', marginLeft: '2px' }}>
                <Phone size="22" color="#2f2f2f"/>
                <div 
                style={{
                    fontSsize: '1.1em',
                    marginLeft: '5px',
                    lineHeight: '1.1em',
                    marginTop: '5px',
                }}> 
                    {phone}
                </div>
            </div>
            )}

            <Description description={description}/>

        {accepted && (<div style={{textAlign: 'right'}}>
            <button
            onClick={() => updateTask(id, 'open')}
            className="btn"
            style={{
            background: 'none',
            fontWeight: 'bold',
            fontSize: '0.8em',
            border: 'none',
            color: '#e85555',
            float: 'right',
            marginTop: '19px',
        }}><Trash2 size="20" color='#e85555' style={{marginTop: '-5px'}} /> {t('tasks.cancel')}</button>
        </div>)
        }
        {!accepted && <button className="btn btn-action" onClick={() => verifyAcceptance(id, 'ongoing')}>{t('localactivist.help_here')}</button>}
        </div>

    );
};
export default TaskItemBox;