import React from 'react';
import {useTranslation} from 'react-i18next';
import './styles.scss';
import taskbox from '../../styles/images/tasksbox.png'
import TaskItemBox from "./taskitembox";
import { useSelector } from 'react-redux';

const mergingTasks = (pArray,mArray) => {
    const tmp = pArray.map((item) => {
        for (let i= 0; i < mArray.length; i++) {
            if (item.id === mArray[i].id) {
                return {...mArray[i], type: item.type};
            }
        }
        return item
    });
    return tmp;
}

const TaskBox = ({ personInNeed }) => {
    const {t, i18n} = useTranslation();
    const {street_name, city, postal_code} = personInNeed;
    const myTasks = useSelector(redux => redux.myTasks);
    const mergedTasks = mergingTasks(personInNeed.tasks, myTasks);
    return (
        <div id="outerTaskBox">
            <div id="taskBox">
                <h2>
                    {t('tasks.help_here')}
                </h2>
                {mergedTasks.map(task => {
                    if (task.status === 'done') return null;
                return <TaskItemBox personInNeed={personInNeed} key={task.id} taskDetails={{...task,street_name, city, postal_code}} />
})}
            </div>
        </div>
    );
};

export default TaskBox;