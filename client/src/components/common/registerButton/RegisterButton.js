import React from "react";
import { useTranslation } from "react-i18next";

import './styles.scss';

const RegisterButton = () => {
    const { t } = useTranslation();

    return (
        <a href="/registration/" type="button" className="btn-helper btn-action">{t('header.register')}</a>
    );
}

export default RegisterButton;