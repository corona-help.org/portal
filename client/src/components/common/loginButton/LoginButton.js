import React from "react";
import { User } from 'react-feather';
import { useTranslation } from "react-i18next";

import './styles.scss';

const LoginButton = (props) => {
    const { onClick } = props;
    const { t } = useTranslation();

    return (
        <button type="button" className="btn-login" onClick={onClick}><User />{t('header.login')}</button>
    );
}

export default LoginButton;