import React from 'react';
import './styles.scss';
import { useSelector } from 'react-redux';
import { Bell } from 'react-feather';
import { useTranslation } from 'react-i18next';

const MyTasksButton = ({setShowMyTasks}) => {
  const myTasks = useSelector(redux => redux.myTasks);
  const onGoingTasks = myTasks.filter(item => item.status === 'ongoing');
  const { t } = useTranslation();

  if (myTasks && myTasks.length > 0 ) {
    return (
      <button className="btn-myTasks btn-action" style={{height: 'auto'}} onClick={()=>setShowMyTasks(true)}>
        <Bell style={{marginRight: '10px', marginTop: '-3px'}}/>
        {t('myTasks.youHaveXTasks', {count: onGoingTasks.length})}
      </button>
    )
  }
  return null;
}

export default MyTasksButton;
