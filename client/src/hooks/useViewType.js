import { useLayoutEffect, useState } from 'react';

export const VIEW_TYPE = {
    DESKTOP: 'd',
    MOBILE: 'm'
};

const MOBILE_BREAKPOINT = 768;

const useViewType = () => {
    const [viewType, setViewType] = useState(VIEW_TYPE.DESKTOP);

    useLayoutEffect(() => {
        const updateViewType = () => {
            if (window.innerWidth < MOBILE_BREAKPOINT) {
                setViewType(VIEW_TYPE.MOBILE);
            }
            else {
                setViewType(VIEW_TYPE.DESKTOP);
            }
        }
        
        window.addEventListener('resize', updateViewType);
        updateViewType();
        return () => window.removeEventListener('resize', updateViewType);
    }, []);

    return viewType;
}

export default useViewType;