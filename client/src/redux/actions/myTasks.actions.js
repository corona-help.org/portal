
import axios from 'axios';
import Swal from 'sweetalert2';

import {API_SERVER} from "../../config";
import {
  GET_MY_TASKS, UPDATE_MY_TASK,
} from './actions.type';
import i18n from 'i18next';


export const getMyTasks = () => (dispatch, getState) => {
  const { user } = getState();
  dispatch({
    type: GET_MY_TASKS,
    payload: user.helper_tasks,
  })
};

export const updateMyTask = (id, status, person_in_need) => async (dispatch, getState) => {
  const updatedTask = await axios.put(API_SERVER+`localactivist/api/v1/tasks/${id}/`, {
    status,
  })
  .then(res => { 
    if ( status=== 'ongoing') {
      Swal.fire(i18n.t('tasks.needToCallForMoreDetails'));
    }
    return res.data;
  })
  .catch(err => {
    return false;
    });
  if (!updatedTask) return;
  const { myTasks } = getState();
  let updatedMyTasks = [];

  updatedMyTasks = myTasks.map(item => {
    if (item.id === id) {
      return {...item, status, person_in_need};
    } else {
      return item;
    }
  })
  if (status ==='ongoing') {
    updatedMyTasks.push({...updatedTask, person_in_need})
  }

  dispatch({
    type: UPDATE_MY_TASK,
    payload: updatedMyTasks,
  })
};