import axios from 'axios';

import {API_SERVER} from "../../config";
import AUTH_USER, { RESET_REDUX, GET_MY_TASKS } from './actions.type';


export const isAuthenticated = () => async (dispatch) => {
  if (!localStorage.getItem('access')) {
    dispatch({ 
      type: AUTH_USER,
      payload: {loggedIn:false},
    });
  } else {
      try {
        const res = await axios.get(API_SERVER + 'localactivist/api/v1/user/');
        if (res.data.count > 0){
            res.data.results[0].loggedIn = true;
            dispatch({ 
              type: AUTH_USER,
              payload: res.data.results[0],
            });
            dispatch({ 
              type: GET_MY_TASKS,
              payload: res.data.results[0].helper_tasks,
            })
        }
        else{
          dispatch({ 
            type: AUTH_USER,
            payload: {loggedIn:false},
          });
        }
    } catch(e) {
      dispatch({ 
        type: AUTH_USER,
        payload: {loggedIn:false},
      });
    }
  }
}

export const login = (form) => async (dispatch) =>{
  const loggedIn = await axios.post(API_SERVER+'localactivist/api/token/', {
    ...form
  })
  .then(res => {
    const { refresh, access } = res.data;
    localStorage.setItem('refresh', refresh);
    localStorage.setItem('access', access);
    dispatch(isAuthenticated());
    return true;
  })
  .catch((error) => error.response.data);
  return loggedIn;
}

export const logout = () => async (dispatch) => {
  localStorage.removeItem('refresh');
  localStorage.removeItem('access');
  dispatch({
    type: RESET_REDUX,
  })
  window.location.href ='/';
}

export default {
  isAuthenticated
};
