import { GET_MAP_TASKS } from "./actions.type"


export const getMapTasks = (tasks) => dispatch => {
  dispatch({
    type: GET_MAP_TASKS,
    payload:tasks,
  })
};

export default getMapTasks;
