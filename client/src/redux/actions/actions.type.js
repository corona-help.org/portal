export const RESET_REDUX = 'RESET_REDUX';
export const GET_MY_TASKS = 'GET_MY_TASKS';
export const SET_MY_TASKS = 'SET_MY_TASKS';
export const GET_PERSONS_IN_NEED = 'GET_PERSONS_IN_NEED';
export const SET_PERSONS_IN_NEED = 'SET_PERSONS_IN_NEED';
export const AUTH_USER = 'AUTH_USER';
export const UPDATE_MY_TASK = 'UPDATE_MY_TASK';
export const GET_MAP_TASKS ='GET_MAP_TASKS';

export default AUTH_USER;