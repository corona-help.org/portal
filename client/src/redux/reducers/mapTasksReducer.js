import { GET_MAP_TASKS }  from '../actions/actions.type';

export default (state = [], action) => {
  switch (action.type) {
    case GET_MAP_TASKS:
      return action.payload || [];
    default:
      return state;
  }
};
