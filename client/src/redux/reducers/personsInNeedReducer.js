import { GET_PERSONS_IN_NEED }  from '../actions/actions.type';

export default (state = [], action) => {
  switch (action.type) {
    case GET_PERSONS_IN_NEED:
      return action.payload || [];
    default:
      return state;
  }
};
