import { combineReducers } from 'redux';
import { RESET_REDUX } from '../actions/actions.type';
import myTasks from './myTasksReducer';
import personsInNeed from './personsInNeedReducer';
import user from './userReducer';
import mapTasks from './mapTasksReducer';

const appReducer = combineReducers({
  myTasks,
  personsInNeed,
  user,
  mapTasks,
});

const rootReducer = (state, action) => {
  if (action.type === RESET_REDUX) {
    return { user: { loggedIn: false }};
  } else {
    return appReducer(state, action)
  }
}

export default rootReducer;