import { GET_MY_TASKS, UPDATE_MY_TASK }  from '../actions/actions.type';

export default (state = [], action) => {
  switch (action.type) {
    case GET_MY_TASKS:
      return action.payload || [];
    case UPDATE_MY_TASK:
      return action.payload || [];
    default:
      return state;
  }
};
