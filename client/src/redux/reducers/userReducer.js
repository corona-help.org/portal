import { AUTH_USER }  from '../actions/actions.type';

export default (state = {}, action) => {
  switch (action.type) {
    case AUTH_USER:
      return action.payload || {};
    default:
      return state;
  }
};
