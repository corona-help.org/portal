import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import translation_de from './translations/de.json';
import translation_en from './translations/en.json';

// Add languages with same format
const resources = {
    de: {
        all: translation_de
    },
    en: {
        all: translation_en
    }
};

i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources,
        defaultNS: 'all',
        lng: "de",
        interpolation: {
            escapeValue: false // react already safes from xss
        }
    });

export default i18n;