import axios from 'axios';
import { API_SERVER } from './config';
import i18n from './i18n';
import Swal from 'sweetalert2';

export const axiosIntercepts = () => {
  axios.interceptors.request.use(async (config) => {
    // if its a request to back-end(contain origin url or a path that stats with /) add auth token
      const token = localStorage.getItem('access');
      config.headers.Authorization = `Bearer ${token}`;
    return config;
  });
  const interceptor = axios.interceptors.response.use(response => response, (error) => {
    // Reject promise if usual error
    if (error.response.status !== 401 || (error.response.status === 401 && error.response.data && error.response.data.detail === 'No active account found with the given credentials')) {
      if ( error.response.status === 405) {
        if (error.response.data.code === 600) {
          Swal.fire({
            title: i18n.t('tasks.error'),
            text: i18n.t('tasks.missingID'),
            icon: 'error',
            confirmButtonText: i18n.t('tasks.verify'),
          }).then(() => window.location.href = API_SERVER+'registration/check_verify');
        } else if (error.response.data.code === 601) {
          Swal.fire({
            title: i18n.t('tasks.error'),
            text: i18n.t('tasks.notSMSVerified'),
            icon: 'error',
            confirmButtonText: i18n.t('tasks.verify'),
          }).then(() => window.location.href = API_SERVER+'registration/check_verify');
        }
      }
      return Promise.reject(error);
    }
    console.log(error.response)
    /*
     * When response code is 401, try to refresh the token.
     * Eject the interceptor so it doesn't loop in case
     * token refresh causes the 401 response
     */
    const refresh = localStorage.getItem('refresh');
    axios.interceptors.response.eject(interceptor);
    return axios.post(API_SERVER+'localactivist/api/token/refresh/', {
      refresh,
    })
    .then(res => {
      const { access } = res.data;
      error.response.config.headers.Authorization = `${access}`;
      localStorage.setItem('access', access);
      return axios(error.response.config);
    })
    .catch((er) => {
      //window.location.href = '/login';
      localStorage.removeItem('access');
      localStorage.removeItem('refresh');
      return Promise.reject(er);
    }).finally(axiosIntercepts);
  });
  return interceptor;
};

export default axiosIntercepts;
