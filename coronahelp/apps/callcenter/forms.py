# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.forms import ModelForm
from django_registration.forms import forms
from ..public.models import LANGUAGES, Languages
from coronahelp.apps.personinneed.models import PersonInNeed


class PersonInNeedForm(ModelForm):

    language = forms.MultipleChoiceField(choices=LANGUAGES, widget=forms.CheckboxSelectMultiple())

    class Meta:
        model = PersonInNeed
        fields = ['first_name',
                  'last_name',
                  'fon',
                  'postal_code',
                  'street_name',
                  'street_number',
                  'city',
                  'state',
                  'country',
                  'email_address',
                  'language']

    def save(self, commit=True):
        """
        Save this form's self.instance object if commit=True. Otherwise, add
        a save_m2m() method to the form which can be called after the instance
        is saved manually at a later time. Return the model instance.
        """
        if self.errors:
            raise ValueError(
                "The %s could not be %s because the data didn't validate." % (
                    self.instance._meta.object_name,
                    'created' if self.instance._state.adding else 'changed',
                )
            )
        self.instance.save()
        for lang in self.cleaned_data['language']:
            print(lang)
            the_lang = Languages.objects.get(name=lang)
            self.instance.language.add(the_lang)
        self.instance.save()
        return self.instance
