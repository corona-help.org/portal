# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
import math
from ..registration.models import User, CATEGORIES
from ..public.models import Languages


class HelpCategories(models.Model):
    name = models.CharField(choices=CATEGORIES, max_length=20)

    def __str__(self):
        return self.name


class PersonInNeed(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    first_name = models.CharField(max_length=100, verbose_name="First Name")
    last_name = models.CharField(max_length=255, verbose_name="Last Name")
    fon = models.CharField(max_length=100, verbose_name="Fon",
                           help_text="Please use format +49(leave out leading 0) 152337182 ")
    postal_code = models.IntegerField(verbose_name="Postal Code")
    street_name = models.CharField(max_length=255, verbose_name="Streetname")
    street_number = models.CharField(max_length=10, verbose_name="House Number")
    city = models.CharField(max_length=255, verbose_name=_("City"))
    state = models.CharField(max_length=255, verbose_name=_("State"))
    country = models.CharField(max_length=255, verbose_name=_("Country"))
    email_address = models.EmailField(null=True, blank=True)
    language = models.ManyToManyField(Languages, related_name="lang_pin", verbose_name=_("Languages"))
    lat = models.FloatField(verbose_name="latitude", null=True, blank=True)
    long = models.FloatField(verbose_name="longitude", null=True, blank=True)

    def __str__(self):
        return self.first_name + self.last_name

    def get_tasks(self):
        tasks = self.tasks.all()
        if tasks.exists():
            return tasks
        else:
            return []

    def get_tasks_links(self):
        tasks = self.tasks.all()
        if len(tasks) > 0:
            the_link_arr = ""
            for i in tasks:
                link = reverse("admin:personinneed_tasks_change", args=[i.id])
                sfe = format_html('<a href="{}">{}</a>, ', link, i.title)
                the_link_arr = the_link_arr + sfe
            return mark_safe(the_link_arr)
        else:
            return ""

    @staticmethod
    def search_by_radius(lat, long, radius, max_results=None):
        """
        For simplicity reasons: Fetching rectangle instead of circle
        https://www.kompf.de/gps/distcalc.html
        lat/long - geo coordinates
        radius   - km
        """
        d_lat = radius / 111.3
        d_long = radius / (111.3 * math.cos(lat * math.pi / 180))
        # return PersonInNeed.objects.filter(
        #     long__range=(long - d_long, long + d_long),
        #     lat__range=(lat - d_lat, lat + d_lat)
        # )[:max_results]
        print(d_long, d_lat)
        return PersonInNeed.objects.filter(
            lat__range=(lat - d_lat, lat + d_lat),
            long__range=(long - d_long, long + d_long),
            tasks__status="open"
        )


class Tasks(models.Model):
    STATUS_CHOICES = (
        ('ongoing', _('currently being worked on')),
        ('done', _('done')),
        ('open', _('open'))
    )

    class TaskChoices(models.TextChoices):
        ONCE = 'once', _('once')
        RECURRING = 'RC', _('recurring')

    created = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=255, verbose_name=_("Task"))
    description = models.TextField(blank=True)
    type = models.CharField(max_length=10, choices=TaskChoices.choices,
                            default=TaskChoices.ONCE, verbose_name=_("Type of the Task"))
    status = models.CharField(choices=STATUS_CHOICES, max_length=30)
    helper = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE, related_name='helper_tasks')
    person_in_need = models.ForeignKey(PersonInNeed, null=True, blank=True, on_delete=models.CASCADE,
                                       related_name='tasks')
    help_topics = models.ForeignKey("HelpCategories", on_delete=models.CASCADE,
                                    verbose_name=_("Help Categories"), null=True)

    def __str__(self):
        return self.title
