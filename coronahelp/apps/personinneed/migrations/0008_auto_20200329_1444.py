# Generated by Django 3.0.4 on 2020-03-29 12:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('personinneed', '0007_auto_20200325_1413'),
    ]

    operations = [
        migrations.AlterField(
            model_name='helpcategories',
            name='name',
            field=models.CharField(choices=[('shopping', 'Einkaufen gehen'), ('animal_care', 'Tierbetreuung'), ('fon_care', 'Betreuung per Telefon'), ('logistics', 'Botengänge'), ('babysitting', 'Kinderbetreuung'), ('other', 'Andere Erledigungen')], max_length=20),
        ),
    ]
