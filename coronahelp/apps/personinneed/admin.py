# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import admin
from .models import PersonInNeed, Tasks, HelpCategories


class TasksInline(admin.StackedInline):
    model = Tasks


class PersonInNeedAdmin(admin.ModelAdmin):
    readonly_fields = ('lat', 'long')
    inlines = [TasksInline, ]
    list_display = ('first_name', 'last_name', 'city', 'get_tasks_links')
    search_fields = ['last_name', 'first_name', 'tasks']


admin.site.register(PersonInNeed, PersonInNeedAdmin)
admin.site.register([Tasks, HelpCategories])
