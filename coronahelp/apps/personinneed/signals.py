from django.conf import settings
from django.db.models.signals import pre_save
from django.dispatch import receiver
import googlemaps

from coronahelp.apps.personinneed.models import PersonInNeed


@receiver(pre_save, sender=PersonInNeed)
def model_Person_in_need_pre_save(sender, **kwargs):
    ###
    # receive geo data before saving the instance
    ###

    if settings.TESTGEO or not settings.DEBUG:
        gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY)
        result = gmaps.geocode(kwargs['instance'].street_name + "," + kwargs['instance'].city)

        kwargs['instance'].lat = result[0]['geometry']['location']['lat']
        kwargs['instance'].long = result[0]['geometry']['location']['lng']
    if settings.DEBUG and not settings.TESTGEO:
        kwargs['instance'].lat = 52.531677
        kwargs['instance'].long = 13.381777
