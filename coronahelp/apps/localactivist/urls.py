# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import re_path, path, include
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views
from .views import LocalActivistsPage, api_initial_data_authorized, \
    TaskAssignSet, UserViewSet, PersonInNeedViewSet

router = routers.DefaultRouter()
router.register(r'tasks', TaskAssignSet, basename='tasks')
router.register(r'user', UserViewSet, basename='user')
router.register(r'personinneed', PersonInNeedViewSet, basename='personinneed')


urlpatterns = [
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('api/v1/initial_data/', api_initial_data_authorized, name="initial_authorized"),
    path('api/v1/', include(router.urls)),
    re_path(r'^$', LocalActivistsPage.as_view(), name='localactivists__main_page'),
]
