# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from coronahelp.apps.localactivist.serializers import PeopleInNeedInitialAuthorized, TaskSerializer, UserSerializer
from coronahelp.apps.personinneed.models import PersonInNeed, Tasks
from coronahelp.apps.registration.models import User
from coronahelp.apps.registration.views import UserIsVerified


class LocalActivistsPage(UserIsVerified, TemplateView):
    '''
    View of the Volunteerlogged in area (maps etc.)
    '''

    template_name = 'public/landing_page.html'


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def api_initial_data_authorized(request):
    '''
    API endpoint for not authorized users on the landing page
    '''

    if request.method == 'GET':
        pin_nextby = PersonInNeed.search_by_radius(float(request.GET.get('lat')),
                                                   float(request.GET.get('lng')), 240, 200)
        pin_serializer = PeopleInNeedInitialAuthorized(pin_nextby, many=True)
        return Response(pin_serializer.data)


@permission_classes([IsAuthenticated])
class TaskAssignSet(ModelViewSet, RetrieveModelMixin, ListModelMixin):
    serializer_class = TaskSerializer
    queryset = Tasks.objects.all()

    def update(self, request, *args, **kwargs):
        # Dealing with status changes:
        # if status is open > dissolve relation to helper
        # if status is ongoing > create relation to helper
        # if status is done > set status to done
        if not request.user.sms_verified:
            return Response({'code': 601},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)
        if not request.user.id_verified:
            return Response({'code': 600},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)

        if request.user.sms_verified and request.user.id_verified:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            instance.helper = request.user
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)

            if request.data.get('status') == 'ongoing':
                instance.helper = request.user
                instance.save()
            if request.data.get('status') == 'open':
                instance.helper = None
                instance.save()

            self.perform_update(serializer)

            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

            return Response(serializer.data)
        else:
            return AuthenticationFailed(code=408)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


@permission_classes([IsAuthenticated])
class PersonInNeedViewSet(ReadOnlyModelViewSet, RetrieveModelMixin):
    """
    A simple ViewSet for listing or retrieving users.
    """
    serializer_class = PeopleInNeedInitialAuthorized
    queryset = PersonInNeed.objects.all()


@permission_classes([IsAuthenticated])
class UserViewSet(ModelViewSet, ReadOnlyModelViewSet, RetrieveModelMixin):
    """
    A simple ViewSet for listing or retrieving users.
    """
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = User.objects.filter(pk=self.request.user.id)
        return user
