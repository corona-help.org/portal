# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.views.generic import TemplateView
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from django.conf import settings
import googlemaps
from urllib import parse

from .serializers import PeopleInNeedInitialNotLoggedIn

from coronahelp.apps.personinneed.models import PersonInNeed


class LandingPage(TemplateView):
    template_name = 'public/home.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def api_initial_data(request):
    '''
    API endpoint for not authorized users on the landing page
    '''
    result = []
    location = parse.unquote(request.GET.get('location'))
    if request.method == 'GET':
        task_types_list = False
        if settings.DEBUG and not settings.TESTGEO:
            pin_nextby = PersonInNeed.search_by_radius(52.531677, 13.281777, 540, 200).distinct()
            geo = {"lat": 52.531677,
                   "lng": 13.281777}
        else:
            gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY)
            location = parse.unquote(request.GET.get('location'))
            task_types_list = False
            result = gmaps.geocode(location)
            pin_nextby = PersonInNeed.search_by_radius(result[0]['geometry']['location']['lat'],
                                                       result[0]['geometry']['location']['lng'], 540, 200).distinct()
            geo = {"lat": result[0]['geometry']['location']['lat'],
                   "lng": result[0]['geometry']['location']['lng']}
        try:
            task_types_list = parse.unquote(request.GET.get('type')).split(",")
        except TypeError:
            pass
        if task_types_list:
            filtered = pin_nextby.filter(tasks__help_topics__name="animal_care").distinct()
            pin_serializer = PeopleInNeedInitialNotLoggedIn(filtered, many=True)
            return Response({"data": pin_serializer.data, "geo": geo})
        else:
            pin_serializer = PeopleInNeedInitialNotLoggedIn(pin_nextby, many=True)
            return Response({"data": pin_serializer.data, "geo": geo})
