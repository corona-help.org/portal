# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import re_path, path
from django.views.generic import TemplateView

from coronahelp.apps.public.views import LandingPage
from .views import api_initial_data


# router = routers.DefaultRouter()
# router.register(r'landingpage', PersonInNeedViewSet, basename="pin_landingpage")

urlpatterns = [
    re_path(r'^$', LandingPage.as_view(), name='landing_page'),

    path('about/', TemplateView.as_view(template_name="public/about.html"), name="about"),
    path('participate/', TemplateView.as_view(template_name="public/participate.html"), name="participate"),
    path('imprint/', TemplateView.as_view(template_name="public/imprint.html"), name="imprint"),
    path('terms-service/', TemplateView.as_view(template_name="public/terms_service.html"), name="terms_service"),
    path('privacy/', TemplateView.as_view(template_name="public/privacy.html"), name="privacy"),
    path('api/v1/initial_data/', api_initial_data, name="initial"),
    # path('api/v1/', include(router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
