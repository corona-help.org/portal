# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
import googlemaps

from .models import User


@receiver(pre_save, sender=User)
def model_user_pre_save(sender, **kwargs):
    ###
    # receive geo data before saving the instance
    ###
    if settings.TESTGEO or not settings.DEBUG:
        try:
            gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY)
            result = gmaps.geocode(kwargs['instance'].street_name + "," + kwargs['instance'].city)

            kwargs['instance'].lat = result[0]['geometry']['location']['lat']
            kwargs['instance'].long = result[0]['geometry']['location']['lng']
        except IndexError:
            pass

    if settings.DEBUG and not settings.TESTGEO:
        kwargs['instance'].lat = 52.531677
        kwargs['instance'].long = 13.381777

    if kwargs['instance'].id is None:
        pass
    else:
        previous = User.objects.get(id=kwargs['instance'].id)
        if previous.id_verified != kwargs['instance'].id_verified:
            if not previous.id_verified:
                dicts = {
                    "first_name": kwargs['instance'].first_name,
                    "last_name": kwargs['instance'].last_name,
                    "cta_link": "https://corona-help.org"
                }  # HTML escaping not appropriate in plaintext
                text_body = render_to_string("emails/account_verified.txt", dicts)
                html_body = render_to_string("emails/account_verified.html", dicts)

                msg = EmailMultiAlternatives(subject=_("Dein Corona-Help.org Benutzerkonto ist jetzt verifiziert!"),
                                             from_email=settings.DEFAULT_FROM_EMAIL,
                                             to=[kwargs['instance'].email], body=text_body)
                msg.attach_alternative(html_body, "text/html")
                msg.send()
