# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import url
from django.urls import include, path, re_path
from django.views.generic import TemplateView
from .forms import HelperForm

from .views import HelperCreateView, SmSverifyerView, show_id_image, CheckVerify, PendingIdState

urlpatterns = [
        url(r'^$',
            HelperCreateView.as_view(form_class=HelperForm),
            name='registration_register'),
        path(r'', include('django_registration.backends.activation.urls')),
        path('', include('django.contrib.auth.urls')),
        path(r'thanks/', TemplateView.as_view(template_name="registration/thankyou.html"), name='thankyou'),
        path(r'smsverify/', SmSverifyerView.as_view(), name='sms_verify_from'),
        path(r'check_verify/', CheckVerify.as_view(), name='check_verify'),
        path(r'not_yet_id_verified/', PendingIdState.as_view(), name='pending_id_verification'),
        re_path(r'id_card/(?P<filename>.*)', show_id_image, name='id_image'),

]
