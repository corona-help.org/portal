# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Create your models here.
# -*- coding: utf-8 -*-

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core.files.storage import FileSystemStorage

from django.core.mail import send_mail
from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _

from django_countries.fields import CountryField

from coronahelp.apps.public.models import Languages
from django.conf import settings

CATEGORIES = (("shopping", _("shopping")),
              ("animal_care", _("animal care")),
              ("fon_care", _("Talk to people on the phone")),
              ("logistics", _("Bring things from A to B")),
              ("babysitting", _("childcare")),
              ("other", _("other")),
              )


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')

        languages = []
        # topics = []
        if extra_fields.get("languages"):
            for lang in extra_fields.get("languages"):
                obj = Languages.objects.get_or_create(name=lang)
                languages.append(obj)
            del extra_fields['languages']
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        for lang in languages:
            user.languages.add(lang[0])
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        # all data ist saved into email that's why we need to copy the data
        # into extrafields
        extra_fields = dict(email)
        email = extra_fields['email']
        if extra_fields['password1'] == extra_fields['password2']:
            password = extra_fields['password1']
        try:
            del extra_fields['email']
            del extra_fields['password1']
            del extra_fields['password2']
        except KeyError:
            pass

        extra_fields.setdefault('is_superuser', False)
        extra_fields.setdefault('is_active', False)
        extra_fields.setdefault('is_staff', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


fs = FileSystemStorage(location=settings.FILEPATH_STORAGE, base_url="/registration/id_card/")


class User(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(_('email'), unique=True)
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=30)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True)
    sms_verifyer_token = models.CharField(null=True, max_length=10)
    is_staff = models.BooleanField(default=False)
    show_id_eligible = models.BooleanField(default=False)
    is_hotline_volunteer = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True)
    fon = models.CharField(max_length=100, verbose_name=_("fon"),
                           help_text=_("Please type in format +49(leave out leading 0) 152337182"))
    postal_code = models.IntegerField(verbose_name=_("Postal Code"), null=True)
    street_name = models.CharField(max_length=255, verbose_name=_("Streetname"))
    street_number = models.CharField(max_length=10, verbose_name=_("House Number"))
    city = models.CharField(max_length=255, verbose_name=_("City"))
    state = models.CharField(max_length=255, verbose_name=_("State"))
    country = CountryField(verbose_name=_("Country"), max_length=100)
    sms_verified = models.BooleanField(default=False)
    identity_number = models.CharField(max_length=100, verbose_name=_("Identity Card Number"))
    identity_card_front = models.FileField(storage=fs, verbose_name=_("Identity Card front site"), help_text=_( "Please provide a selfie together with the f r o n t  site of your id card. Has to be readable.")) # noqa
    identity_card_back = models.FileField(storage=fs, verbose_name=_("Identity Card back site"), help_text=_("Please provide a selfie together with the    b a c k site of your id card. Has to be readable.")) # noqa
    id_verified = models.BooleanField(default=False)
    languages = models.ManyToManyField(Languages, verbose_name=_("Which languages are you speaking?"))
    blocked = models.BooleanField(default=False)
    number_of_activation_sms_sent = models.SmallIntegerField(default=1)
    lat = models.CharField(max_length=100, verbose_name="latitude", null=True, blank=True)
    long = models.CharField(max_length=100, verbose_name="longitude", null=True, blank=True)
    email_notifications = models.BooleanField(default=False, verbose_name="", blank=True, help_text=_("Activate email notifications if someone needs help in my area (~5km)")) # noqa
    sms_notifications = models.BooleanField(default=False, verbose_name="", blank=True, help_text=_("Activate SMS notifications if someone needs help in my area (~5km)")) # noqa
    gdpr_accepted = models.BooleanField(verbose_name=_("Accept Privacy Terms"), default=False)
    service_agreement_accepted = models.BooleanField(verbose_name=_("Accept Service Agreement"), default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_my_open_tasks(self):
        return self.helper_tasks.all().filter(status='ongoing')
