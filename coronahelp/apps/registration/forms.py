# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django.utils.safestring import mark_safe
from django_registration.forms import RegistrationForm, forms
from .models import User
from ..public.models import LANGUAGES
from django.utils.translation import ugettext_lazy as _


class HelperForm(RegistrationForm):
    languages = forms.MultipleChoiceField(choices=LANGUAGES,
                                          widget=forms.CheckboxSelectMultiple(attrs={'class': 'col-lg-11'}))
    first_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'col-8 col-sm-8 col-lg-11'}), label=_("Vorname"))
    last_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'col-8 col-sm-8 col-lg-11'}), label=_("Nachname"))
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={'class': 'col-8 col-sm-8 col-lg-11'}), label=_("Email-Adresse"))
    fon = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'col-8 col-sm-8 col-lg-11', 'placeholder': '+49 176 ....'}), label=_("Telefonnummer"))
    city = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'col-8 col-sm-8 col-lg-11'}), label=_("Stadt"))
    street_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'col-8 col-sm-8 col-lg-11'}), label=_("Straßenname"))
    street_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'col-8 col-sm-8 col-lg-11'}), label=_("Hausnummer"))  # noqa
    postal_code = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'col-8 col-sm-8 col-lg-11'}), label=_("Postleitzahl"))  # noqa
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={}), label=_("Passwort"))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={}), label=_("Passwort wiederholen"))
    identity_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'col-12 col-sm-12 col-lg-12'}), label=_("Ausweisnummer"))  # noqa
    identity_card_front = forms.FileField(widget=forms.FileInput(attrs={'class': 'col-12 col-sm-12 col-lg-12'}), label=_(">> Foto von Dir, wie Du Deinen Ausweis (Voderseite) hochhältst"))  # noqa
    identity_card_back = forms.FileField(widget=forms.FileInput(
        attrs={'class': 'col-12 col-sm-12 col-lg-12'}),
        label=_(">> Foto von Dir, wie Du Deinen Ausweis (Rückseite) hochhältst"))
    sms_notifications = forms.BooleanField(
        widget=forms.CheckboxInput(), required=False, initial=False,
        label=_("Aktiviere SMS-Benachrichtigungen wenn jemand in meiner Nähe Hilfe braucht (5km Umkreis)")) # noqa
    email_notifications = forms.BooleanField(widget=forms.CheckboxInput(), required=False, initial=False,
                                             label=_("Aktiviere Emailbenachrichtigungen wenn jemand in meiner Nähe Hilfe braucht (~5km)")) # noqa
    gdpr_accepted = forms.BooleanField(widget=forms.CheckboxInput(),
                                       label=mark_safe(_('Ich stimme den der <a target="_blank" href="/privacy/">Datenschutzbestimmung</a> zu'))) # noqa
    service_agreement_accepted = forms.BooleanField(widget=forms.CheckboxInput(),
                                                    label=mark_safe(_('Ich stimme den <a target="_blank" href="/terms-service/">Nutzungsbestimmungen</a> zu'))) # noqa
    state = forms.CharField(widget=forms.TextInput(attrs={'class': 'col-8 col-sm-8 col-lg-11'}), label=_("Bundesland"))

    class Meta(RegistrationForm.Meta):
        model = User
        fields = [
                  'identity_card_front',
                  'identity_card_back',
                  'first_name',
                  'last_name',
                  'identity_number',
                  'email',
                  'fon',
                  'street_name',
                  'street_number',
                  'postal_code',
                  'city',
                  'state',
                  'country',
                  'languages',
                  'email_notifications',
                  'sms_notifications',
                  'gdpr_accepted',
                  'service_agreement_accepted']

    def clean_email_notifications(self):
        email_notifications = self.cleaned_data['email_notifications']

        return email_notifications

    def clean_sms_notifications(self):
        sms_notifications = self.cleaned_data['sms_notifications']
        return sms_notifications

    def clean(self):
        if not self.cleaned_data.get('email_notifications'):
            self.cleaned_data['email_notifications'] = False
            print('here we are')
        if not self.cleaned_data.get('sms_notifications'):
            self.cleaned_data['sms_notifications'] = False
        gdpr_accepted = self.cleaned_data.get('gdpr_accepted')
        service_agreement_accepted = self.cleaned_data.get('service_agreement_accepted')
        if gdpr_accepted and service_agreement_accepted:
            return self.cleaned_data
        else:
            self.add_error(None,
                           ValidationError(_("Die Datenschutzbestimmung und die "
                                             "Nutzungsbestimmungen müssen angenommen werden. ")))


class SmSverifyForm(ModelForm):
    sms_token = forms.CharField(max_length=10, label=_("Bitte gebe deinen SMS Code ein"))

    class Meta:
        model = User
        fields = ["sms_token"]

    def clean(self):

        sms_token = self.cleaned_data.get('sms_token')
        if sms_token == self.instance.sms_verifyer_token:
            return self.cleaned_data
        else:
            self.add_error(None, ValidationError(_("Token is invalid")))
