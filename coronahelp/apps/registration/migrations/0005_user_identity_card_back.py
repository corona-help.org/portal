# Generated by Django 3.0.4 on 2020-04-04 12:00

import django.core.files.storage
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0004_auto_20200404_1359'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='identity_card_back',
            field=models.FileField(default='djkdj.jpg', storage=django.core.files.storage.FileSystemStorage(base_url='/registration/id_card/', location='/Users/doriancantzen/workspace/ownprojects/coronahelp/media/'), upload_to=''),
            preserve_default=False,
        ),
    ]
